import { defineConfig } from "cypress";

export default defineConfig({
  //video: true, // Record failing cases
  // defaultCommandTimeout: 5000, // how much wait to find elements
  e2e: {
    baseUrl: "http://localhost:5173/", // In test, now on "/about" is enough to give to .visit()
    setupNodeEvents(on, config) {
      // implement node event listeners here
      on("task", {
        seedDatabase(filename) {
          // Run nodeJS code
          return filename;
        },
      });
    },
  },
});
