/// <reference types="cypress" />

describe("share location", () => {
  beforeEach(() => {
    cy.clock();
    cy.fixture("user-location.json").as("userLocation");
    cy.visit("/forth").then((win) => {
      cy.get("@userLocation").then((fakePosition) => {
        cy.stub(win.navigator.geolocation, "getCurrentPosition")
          .as("getUserPosition")
          .callsFake((cb) => {
            setTimeout(() => {
              cb(fakePosition);
            }, 100);
          });
      });
      // Falls response, with 100ms shift

      cy.stub(win.navigator.clipboard, "writeText")
        .as("saveToClipboard")
        .resolves();
      cy.spy(win.localStorage, "setItem").as("storeLocation");
      cy.spy(win.localStorage, "getItem").as("getStoredLocation");
    });
  });
  it("should fetch the user location", () => {
    cy.get('[data-cy="get-loc-btn"]').click();
    cy.get("@getUserPosition").should("have.been.called");
    cy.get('[data-cy="get-loc-btn"]').should("be.disabled");
    cy.get('[data-cy="actions"]').should("contain", "Location fetched");
  });

  it("should share the user location", () => {
    cy.get('[data-cy="name-input"]').type("Richard");
    cy.get('[data-cy="get-loc-btn"]').click();
    cy.get('[data-cy="share-loc-btn"]').click();
    cy.get("@saveToClipboard").should("have.been.called");
    cy.get("@userLocation").then((fakePosition) => {
      const { latitude, longitude } = fakePosition.coords;
      cy.get("@saveToClipboard").should(
        "have.been.calledWithMatch",
        new RegExp(`${latitude}.*${longitude}.*${encodeURI("Richard")}`)
      );

      // cy.get("@storeLocation").should(
      //   "have.been.calledWithMatch",
      //   new RegExp(`${latitude}.*${longitude}.*${encodeURI("Richard")}`)
      // );
    });

    // Checking the response with regex
    // encodeURI: encode the string as the system does

    cy.get("@storeLocation").should("have.been.called");
    cy.get('[data-cy="share-loc-btn"]').click();
    // cy.get("@getStoredLocation").should("have.been.called");

    // Shift clock with 2s
    cy.tick(2000);
    cy.get('[data-cy="info-message"]').should("not.be.visible");
  });
});
