/// <reference types="Cypress" />

describe("tasks page", () => {
  it("should render the main image", () => {
    cy.visit("/second");
    cy.get(".main-header img");
    cy.get(".main-header").find("img"); // Same as above
  });

  it("should display the page title", () => {
    cy.visit("/second");
    cy.get("h1").should("have.length", 1);
    cy.get(".main-header h1").contains("React Tasks");
  });
});

describe("tasks management", () => {
  it("should open and close the new task modal", () => {
    cy.visit("/second");
    cy.contains("Add Task").click();
    cy.get(".backdrop").should("exist");
    cy.get("dialog.modal").should("exist");

    cy.get(".backdrop").click({ force: true });
    // Force bc modal is above it (it clicks in the middle of the component)
    cy.get(".backdrop").should("not.exist");
    cy.get("dialog.modal").should("not.exist");

    cy.contains("Add Task").click();
    cy.contains("Cancel").click();
    cy.get(".backdrop").should("not.exist");
    cy.get("dialog.modal").should("not.exist");
  });

  it("should create a new task", () => {
    cy.visit("/second");
    cy.contains("Add Task").click();
    cy.get("#title").type("New Task");
    cy.get("#summary").type("Some description");
    cy.get(".modal").contains("Add Task").click();
    cy.get(".backdrop").should("not.exist");
    cy.get("dialog.modal").should("not.exist");

    cy.get("li.task").should("have.length", 1);
    cy.get("li.task h2").contains("New Task");
    cy.get("li.task p").contains("Some description");
  });

  it("should validate user input", () => {
    cy.visit("/second");
    cy.contains("Add Task").click();
    cy.get(".modal").contains("Add Task").click();
    cy.contains("Please provide values");
  });

  it("should filter tasks", () => {
    cy.visit("/second");
    cy.contains("Add Task").click();
    cy.get("#title").type("New Task");
    cy.get("#summary").type("Some description");
    cy.get("#category").select("urgent"); // Can use the dropdown value as well
    cy.get(".modal").contains("Add Task").click();

    cy.get("li.task").should("have.length", 1);
    cy.get("#filter").select("moderate");
    cy.get("li.task").should("have.length", 0);
    cy.get("#filter").select("urgent");
    cy.get("li.task").should("have.length", 1);
    cy.get("#filter").select("all");
    cy.get("li.task").should("have.length", 1);
  });

  it("should add multiple tasks", () => {
    cy.visit("/second");
    cy.contains("Add Task").click();
    cy.get("#title").type("Task 1");
    cy.get("#summary").type("First task");
    cy.get(".modal").contains("Add Task").click();
    cy.get("li.task").should("have.length", 1);

    cy.contains("Add Task").click();
    cy.get("#title").type("Task 2");
    cy.get("#summary").type("Second task");
    cy.get(".modal").contains("Add Task").click();
    cy.get("li.task").should("have.length", 2);

    // cy.get("li.task").first();
    // cy.get("li.task").last();
    cy.get("li.task").eq(0).contains("First task"); // .first()
    cy.get("li.task").eq(1).contains("Second task"); // .last()
  });
});
