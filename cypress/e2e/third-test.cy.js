/// <reference types="Cypress" />

describe(
  "page navigation",
  // Can set config locally by section or by test case
  { defaultCommandTimeout: 5000 /*browser: chrome*/ },
  () => {
    it("should navigate between pages", { defaultCommandTimeout: 6000 }, () => {
      cy.visit("/third");

      cy.get('[data-cy="header-about-link"]').click();
      cy.location("pathname").should("eq", "/third/about");

      cy.go("back"); // trigger browser Back button
      cy.location("pathname").should("eq", "/third");

      cy.get('[data-cy="header-about-link"]').click();
      cy.get('[data-cy="header-home-link"]').click();
      cy.location("pathname").should("eq", "/third");
    });
  }
);

describe("contact form", () => {
  // Runs only once, before all tests
  before(() => {
    cy.task("seedDatabase", "filename.csv").then((returnValue) => {
      // can use returnValue
    });
  });
  // Runs before every test
  beforeEach(() => {
    cy.visit("/third/about");
    // Seeding a database
  });
  // Runs after every test
  afterEach(() => {});
  // Runs after all test (only once)
  after(() => {});

  it("should submit the form", () => {
    cy.get('[data-cy="contact-btn-submit"]').then((el) => {
      expect(el.attr("disabled")).to.be.undefined;
      expect(el.text()).to.contain("Send Message"); // OR .to.eq("Send Message")
    });
    cy.getByCyId("contact-input-message").type("Hello World"); // Using Custom Query
    cy.get('[data-cy="contact-input-name"]').type("Richy");
    cy.get('[data-cy="contact-input-email"]').type("test@example.com{enter}");

    cy.get('[data-cy="contact-btn-submit"]').as("submitBtn"); // Creating an alias
    // cy.get("@submitBtn").click();
    cy.get("@submitBtn").contains("Sending...").and("have.attr", "disabled"); // same as .should()
  });

  it("should validate the form input", () => {
    cy.submitForm(); // Using global Command
    cy.get('[data-cy="contact-btn-submit"]').then((el) => {
      expect(el).to.not.have.attr("disabled");
      expect(el.text()).to.not.equal("Sending...");
    });
    cy.get('[data-cy="contact-btn-submit"]').contains("Send Message");

    cy.get('[data-cy="contact-input-message"]').blur();
    cy.get('[data-cy="contact-input-message"]')
      .parent()
      .should((el) => {
        expect(el.attr("class")).contains("invalid");
      });

    cy.get('[data-cy="contact-input-name"]').focus().blur();
    cy.get('[data-cy="contact-input-name"]')
      .parent()
      .should((el) => {
        expect(el.attr("class")).contains("invalid");
      });

    // Can create a screenshot anytime
    // cy.screenshot();

    cy.get('[data-cy="contact-input-email"]').focus().blur();
    cy.get('[data-cy="contact-input-email"]')
      .parent()
      .should("have.attr", "class")
      .and("match", /invalid/); // Other solution
  });
});
