import FirstApp from "./pages/FirstApp";
import SecondApp from "./pages/SecondApp";
import Home from "./pages/Home";
import About from "./pages/About";
import { Navigate, Route, Routes } from "react-router-dom";
import ForthApp from "./pages/ForthApp";

function App() {
  return (
    <>
      <main className="mx-6 py-6">
        <Routes>
          <Route path="/" element={<FirstApp />} />
          <Route path="/second" element={<SecondApp />} />
          <Route path="/third" element={<Home />} />
          <Route path="/third/about" element={<About />} />
          <Route path="/forth" element={<ForthApp />} />
          <Route path="/*" element={<Navigate to="/" />} />
        </Routes>
      </main>
    </>
  );
}

export default App;
