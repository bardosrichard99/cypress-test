import React from "react";
import Header from "../components/HeaderSec";
import CourseGoals from "../components/CourseGoals";

const FirstApp = () => {
  return (
    <>
      <Header />
      <main>
        <CourseGoals />
      </main>
    </>
  );
};

export default FirstApp;
