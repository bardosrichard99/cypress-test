import * as React from "react";

const ForthApp = () => {
  const [location, setLocation] = React.useState(undefined);
  const [userName, setUserName] = React.useState();

  const getUserLocation = () => {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          setLocation({
            lat: position.coords.latitude,
            lng: position.coords.longitude,
            url: `https://www.bing.com/maps?cp=${position.coords.latitude}~${position.coords.longitude}&lvl=15&style=r`,
          });
        },
        () => {
          // displayInfoMessage(
          //   "Your browser or permission settings do not allow location fetching."
          // );
        }
      );
    } else {
      // displayInfoMessage(`
      //   "Your browser or permission settings do not allow location fetching."
      // );
    }
  };

  const shareLocation = () => {
    // Use clipboard API to copy the location to the clipboard
    // if (userName.trim() === "" || location.lat === 0 || location.lng === 0) {
    //   document.getElementById("error").textContent =
    //     "Please enter your name and get your location first!";
    //   return;
    // }

    // document.getElementById("error").textContent = "";

    // if (storedUrl) {
    //   copyToClipboard(storedUrl, "Stored location URL copied to clipboard.");
    //   return;
    // }

    location.url += `&sp=point.${location.lat}_${location.lng}_${encodeURI(
      userName
    )}`;

    localStorage.setItem(userName, location.url);
    copyToClipboard(location.url, "Location URL copied to clipboard.");
  };

  const copyToClipboard = (data, infoText) => {
    if ("clipboard" in navigator) {
      navigator.clipboard
        .writeText(data)
        .then
        // function () {
        //   displayInfoMessage(infoText);
        // },
        // function () {
        //   displayInfoMessage("Failed to copy location URL to clipboard.");
        // }
        ();
    }
  };

  // let existingTimer;

  // const displayInfoMessage = (message) => {
  //   if (existingTimer) {
  //     clearTimeout(existingTimer);
  //   }
  //   const infoMsg = document.getElementById("info-message");
  //   infoMsg.querySelector("p").textContent = message;
  //   infoMsg.classList.add("visible");
  //   existingTimer = setTimeout(() => {
  //     infoMsg.classList.remove("visible");
  //   }, 2000);
  // };

  return (
    <div>
      <h1>SnapLocation</h1>
      <p>
        <label htmlFor="name">Your name</label>
        <input
          type="text"
          id="name"
          name="name"
          data-cy="name-input"
          value={userName}
          onChange={(e) => setUserName(e.target.value)}
        />
      </p>
      <div>
        <button
          type="button"
          onClick={() => getUserLocation()}
          disabled={location}
          data-cy="get-loc-btn"
        >
          Get Location
        </button>
        <p data-cy="actions">{location ? "Location fetched" : ""}</p>
        <button
          data-cy="share-loc-btn"
          disabled={!location}
          onClick={() => shareLocation()}
        >
          Share Link
        </button>
      </div>
      <p data-cy="info-message" id="error" className="visible: none"></p>
    </div>
  );
};

export default ForthApp;
