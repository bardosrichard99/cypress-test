import Header from "../components/Header";

function HomePage() {
  return (
    <>
      <Header />
      <div className="center">
        <h1>Home Page</h1>
      </div>
    </>
  );
}

export default HomePage;
